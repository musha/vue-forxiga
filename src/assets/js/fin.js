// console.log('fin page');

export default {
    data() {
        return {
            //
        };
    },
    mounted() {
        var context = this;
        //
        var tl = new TimelineMax();
        tl.to($('.page-fin'), 0.4, { onComplete: function() {
            title();
        }});

        // title 
        function title() {
            var tl = new TimelineMax({ delay: 0.4, onComplete: function() {
                $('.page-fin .box-text').fadeIn('fast');
            } });
            tl.to($('.page-fin .title1'), 1.4, { 'width': '50%', 'left': '0.04%', ease: Power1.easeOut}, 0);
            tl.to($('.page-fin .title2'), 1.4, { 'width': '50%', ease: Power1.easeOut}, 0);
        };
    },
    methods: {
        home() {
            this.$router.replace('/home');
        },
        share() {
            $('.page-fin .modal-share').fadeIn('fast');
        },
        close() {
            $('.page-fin .modal-share').fadeOut('fast');
        }
    }
};
