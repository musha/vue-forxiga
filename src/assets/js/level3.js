// console.log('level3 page');

export default {
    data() {
        return {
            //
        };
    },
    mounted() {
        var context = this;
        //
        var tl = new TimelineMax();
        tl.to($('.page-level3'), 0.4, { onComplete: function() {
            $('.page-level3 .text1').fadeIn('fast', function() {
                hammer1();
            });
        }});

        tl.to($('.page-level3'), 0.02, { onComplete: function() {
            $('.page-level3 .heart').attr('src', require('../img/level3/heart2.png'));
        }}, '+=1.8');

        tl.to($('.page-level3'), 0.2, { onComplete: function() {
            mouseOut3();
        }}, '+=0.2');

        // hammer
        var canHammer = 1;
        function hammer1() {
            var tl = new TimelineMax({ delay: 0.2, repeat: 5, yoyo: true, onComplete: function() {
                hammer2();
            }});
                tl.to($('.page-level3 .box-hammer'), 0.2, { rotation: 30, ease: Power1.easeIn, onStart: function() {
                    if(canHammer == 0) {
                        tl.kill();
                    };
                }});
                tl.to($('.page-level3 .box-fulu1 .box-fuluheart'), 0.1, { rotation: 4, ease: Power4.easeIn}, 0.1);
                tl.to($('.page-level3 .line'), 0.1, { autoAlpha: 1, ease: Power1.easeIn}, 0.1);
        };
        function hammer2() {
            var tl = new TimelineMax({ repeat: 5, yoyo: true, onComplete: function() {
                hammer3();
            }});
                tl.to($('.page-level3 .box-hammer'), 0.16, { rotation: 30, ease: Power1.easeIn, onStart: function() {
                    if(canHammer == 0) {
                        tl.kill();
                    };
                }});
                tl.to($('.page-level3 .box-fulu1 .box-fuluheart'), 0.08, { rotation: 4, ease: Power4.easeIn}, 0.08);
                tl.to($('.page-level3 .line'), 0.08, { autoAlpha: 1, ease: Power1.easeIn}, 0.08);
        };
        function hammer3() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true});
                tl.to($('.page-level3 .box-hammer'), 0.12, { rotation: 30, ease: Power1.easeIn, onStart: function() {
                    if(canHammer == 0) {
                        tl.kill();
                    };
                }});
                tl.to($('.page-level3 .box-fulu1 .box-fuluheart'), 0.06, { rotation: 4, ease: Power4.easeIn}, 0.06);
                tl.to($('.page-level3 .line'), 0.06, { autoAlpha: 1, ease: Power1.easeIn}, 0.06);
        };

        // mouse out
        function mouseOut3() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level3 .box-mouse1'), 0.6, { 'top': '0rem', ease: Power4.easeOut, onComplete: function() {
                eyes3($('.page-level3 .eyes'));
                $('.page-level3 .help').fadeIn('fast', function() {
                    helpShake3($('.page-level3 .help'));
                });
            }});
        };

        // eyes
        function eyes3(el) {
            var tl = new TimelineMax({ repeat: 1});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/common/close.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/common/open.png'));
            }});
        };

        function helpShake3(el) {
            var tl = new TimelineMax({ delay: 0.2, repeat: -1, repeatDelay: 1});
            tl.to(el, 0.35, {'right': '+=2%', ease: Power0.easeOut, repeat: 1, yoyo: true, onStart: function() {
                if(canHelp == 0) {
                    tl.kill();
                };
            }});
            tl.to(el, 0.35, {'right': '+=2%', ease: Power0.easeOut, repeat: 1, yoyo: true});
        };

        // help
        var canHelp = 1; 
        $('.page-level3 .help').off('click');
        $('.page-level3 .help').on('click', function() {
            if(canHelp == 1) {
                canHammer = 0;
                canHelp = 0;
                $('.page-level3 .help').fadeOut('fast');
                mouseIn3();
            };
        });

        // mouse in
        function mouseIn3() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level3 .box-mouse1'), 0.6, { 'top': '1.74rem', ease: Power4.easeOut, onComplete: function() {
                tl.to($('.page-level3 .box-cave'), 0.6, {scale: 0, ease: Power4.easeOut, onComplete: function() {
                    mouseAttack($('.page-level3 .box-attack1'), $('.page-level3 .box-attack2'), $('.page-level3 .box-attack3'));
                }});
            }});
        };

        // mouse come
        function mouseAttack(el1, el2, el3) {
            var tl = new TimelineMax({ delay: 0.4, onComplete: function() {
                mouseLeave3();
            } });
            tl.to(el1, 1.6, { 'top': '2.88rem', 'left': '100%', ease: Power4.easeOut, onComplete: function() {
                el1.hide();
            }});
            tl.to($('.page-level3 .crack'), 0.15, { autoAlpha: 1}, '-=0.8');
            tl.to(el2, 1.6, { 'top': '7.5rem', 'right': '100%', ease: Power4.easeOut, onComplete: function() {
                el2.hide();
            }}, '-=0.6');
            tl.to($('.page-level3 .crack'), 0.15, { onComplete: function() {
                $('.page-level3 .crack').attr('src', require('../img/level3/crack2.png'));
            }}, '-=0.8');
            tl.to(el3, 0.8, { 'top': '2.94rem', 'left': '-27.8%', ease: Power4.easeIn}, '-=0.6');
            tl.to($('.page-level3 .box-attack3 .speed3'), 0.15, {autoAlpha: 0, ease: Power4.easeIn});
            tl.to($('.page-level3 .box-attack3 .line2'), 0.04, {autoAlpha: 1, ease: Power1.easeIn, repeat: 1, yoyo: true}, '-=0.08');
            tl.to(el3, 0.6, {top: '+=0.1rem', ease: Power0.easeOut, repeat: 3, yoyo: true});
            tl.to($('.page-level3 .crack'), 0.15, { onComplete: function() {
                $('.page-level3 .crack').attr('src', require('../img/level3/crack3.png'));
            }}, '-=2');
        };

        // mouse leave
        function mouseLeave3() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level3 .box-hammer'), 0.6, { 'top': '+=20rem', ease: Power4.easeIn, onComplete: function() {
                $('.page-level3 .box-hammer').hide();
            }});
            tl.to($('.page-level3 .box-attack3'), 0.6, { 'top': '18.94rem', 'left': '110.4%', ease: Power4.easeIn, onComplete: function() {
                $('.page-level3 .box-attack3').hide();
            }});
            tl.to($('.page-level3'), 0.2, { onComplete: function() {
                $('.page-level3 .text1').fadeOut(400);
                $('.page-level3 .box-fulu1').fadeOut(400);
            }}, '+=1');
            tl.to($('.page-level3'), 0.2, { onComplete: function() {
                $('.page-level3 .re').fadeIn(400, function() {
                    fuluReShake3($('.page-level3 .re .box-fulu2 .fulu2'));
                    ingot($('.page-level3 .re .ingot'));
                });
            }}, '+=0.4');
            tl.to($('.page-level3'), 0.2, { onComplete: function() {
                $('.page-level3 .re .box-tip').fadeIn('fast', function() {
                    tipTwinkle3();
                });
            }}, '+=1');
        };

        // fulu shake
        function fuluReShake3(el) {
            var tl = new TimelineMax({ repeat: -1, delay: 0.2 });
            tl.to(el, 0.25, { rotation: 6, ease: Power1.easeOut, repeat: 1, yoyo: true});
            tl.to(el, 0.25, { rotation: -6, ease: Power1.easeOut, repeat: 1, yoyo: true});
        };

        // ingot
        function ingot(el) {
            var tl = new TimelineMax({ repeat: -1, delay: 0.2 });
            tl.to(el, 0.4, { onComplete: function() {
                el.attr('src', require('../img/level3/ingot2.png'));
            }});
            tl.to(el, 0.4, { onComplete: function() {
                el.attr('src', require('../img/level3/ingot1.png'));
            }});
        };

        // tip
        function tipTwinkle3() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true });
            tl.to($('.page-level3 .re .next'), 0.4, {autoAlpha: 1});
        };
    },
    methods: {
        prev() {
            this.$router.replace('/level2');
        },
        next() {
            this.$router.replace('/level4');
        }
    }
};
