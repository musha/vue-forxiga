// console.log('level4 page');

export default {
    data() {
        return {
            //
        };
    },
    mounted() {
        var context = this;
        //
        var tl = new TimelineMax();
        tl.to($('.page-level4'), 0.4, { onComplete: function() {
            $('.page-level4 .text1').fadeIn('fast', function() {
                ghostOut();
            });
        }});

        // ghost out
        function ghostOut() {
            var tl = new TimelineMax({ delay: 0.2 });
            tl.to($('.page-level4 .ghost'), 2, { scale: 1, 'top': '0.88rem', 'left': '2.6%', ease: Power4.easeOut, onComplete: function() {
                ghostFloat();
                mouseOut4();
            }});
        };

        // ghost float
        function ghostFloat() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true });
            tl.to($('.page-level4 .ghost'), 0.6, { 'top': '+=0.3rem', ease: Power0.easeOut});
        };

        // mouse out
        function mouseOut4() {
            var tl = new TimelineMax({ delay: 0.2 });
            tl.to($('.page-level4 .box-mouse1'), 0.6, { 'top': '0rem', ease: Power4.easeOut, onComplete: function() {
                eyes4($('.page-level4 .eyes'));
                $('.page-level4 .help').fadeIn('fast', function() {
                    helpShake4($('.page-level4 .help'));
                });
            }});
        };

        // eyes
        function eyes4(el) {
            var tl = new TimelineMax({ repeat: 1});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/common/close.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/common/open.png'));
            }});
        };

        function helpShake4(el) {
            var tl = new TimelineMax({ delay: 0.2, repeat: -1, repeatDelay: 1});
            tl.to(el, 0.35, {'right': '+=2%', ease: Power0.easeOut, repeat: 1, yoyo: true, onStart: function() {
                if(canHelp == 0) {
                    tl.kill();
                };
            }});
            tl.to(el, 0.35, {'right': '+=2%', ease: Power0.easeOut, repeat: 1, yoyo: true});
        };

        // help
        var canHelp = 1; 
        $('.page-level4 .help').off('click');
        $('.page-level4 .help').on('click', function() {
            if(canHelp == 1) {
                canHelp = 0;
                $('.page-level4 .help').fadeOut('fast');
                mouseIn4();
            };
        });

        // mouse in
        function mouseIn4() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level4 .box-mouse1'), 0.6, { 'top': '1.74rem', ease: Power4.easeOut, onComplete: function() {
                tl.to($('.page-level4 .box-cave'), 0.6, {scale: 0, ease: Power4.easeOut, onComplete: function() {
                    mouseCome4();
                }});
            }});
        };

        // mouse come
        function mouseCome4() {
            var tl = new TimelineMax({ delay: 0.4 });
            tl.to($('.page-level4 .box-run'), 0.8, { 'right': '70%', ease: Power4.easeOut, onStart: function() {
                mouseRun($('.page-level4 .run'));
            }});
            tl.to($('.page-level4 .box-run'), 0.2, { onStart: function() {
                $('.page-level4 .ghost').fadeOut('fast');
                fuluTurn($('.page-level4 .box-fulu1 .fulu1'));
                lineTurn($('.page-level4 .box-fulu1 .turn'));
                sugar4();
            }}, '-=0.8');
            tl.to($('.page-level4 .box-run .line'), 0.2, {autoAlpha: 1, repeat: 1, yoyo: true}, 0.1);
            tl.to($('.page-level4 .box-run'), 0.1, { scaleX: -1}, '-=0.2');
            tl.to($('.page-level4 .box-run'), 0.8, { 'right': '-33.2%', ease: Power4.easeIn, onComplete: function() {
                $('.page-level4 .box-run').hide();
            }});
            tl.to($('.page-level4 .box-run .line'), 0.2, {autoAlpha: 1, repeat: 1, yoyo: true}, 1.3);
        };

        // mouse run
        function mouseRun(el) {
            var tl = new TimelineMax({ repeat: 11 });
            tl.to(el, 0.06, { onComplete: function() {
                el.attr('src', require('../img/level4/run2.png'));
            }});
            tl.to(el, 0.06, { onComplete: function() {
                el.attr('src', require('../img/level4/run3.png'));
            }});
            tl.to(el, 0.06, { onComplete: function() {
                el.attr('src', require('../img/level4/run1.png'));
            }});
        };

        // fulu turn
        function fuluTurn(el) {
            var tl = new TimelineMax({ delay: 0.4});
            tl.to(el, 0.25, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.25, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.25, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.05, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.05, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.05, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.05, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.05, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.05, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.2, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
            tl.to(el, 0.25, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu2.png'));
            }});
            tl.to(el, 0.25, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu3.png'));
            }});
            tl.to(el, 0.25, { onComplete: function() {
                el.attr('src', require('../img/level4/fulu1.png'));
            }});
        };

        // line turn
        function lineTurn(el) {
            var tl = new TimelineMax({ repeat: 13, delay: 0.4, onComplete: function() {
                el.fadeOut(function() {
                    reShow();
                });
            } });
            tl.to(el, 0.1, { autoAlpha:1, onComplete: function() {
                el.attr('src', require('../img/level4/turn2.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/turn3.png'));
            }});
            tl.to(el, 0.1, { onComplete: function() {
                el.attr('src', require('../img/level4/turn1.png'));
            }});
        };

        // sugar
        function sugar4() {
            var tl = new TimelineMax({ delay: 0.6});
            tl.to($('.page-level4 .box-sugar1'), 0.2, {'display': 'block'}, 0);
            tl.to($('.page-level4 .sugar1'), 0.4, {'top': '-0.96rem', 'left': '-220%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level4 .box-sugar1').hide();
            }}, 0.02);
            tl.to($('.page-level4 .line1'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 0.02);

            tl.to($('.page-level4 .box-sugar2'), 0.2, {'display': 'block'}, 1);
            tl.to($('.page-level4 .sugar2'), 0.4, {'top': '-2.6rem', 'left': '-200%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level4 .box-sugar2').hide();
            }}, 1.02);
            tl.to($('.page-level4 .line2'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 1.02);

            tl.to($('.page-level4 .box-sugar3'), 0.2, {'display': 'block'}, 2);
            tl.to($('.page-level4 .sugar3'), 0.4, {'top': '-2.4rem', 'right': '-320%', ease: Power4.easeOut, onComplete: function() {
                $('.page-level4 .box-sugar3').hide();
            }}, 2.02);
            tl.to($('.page-level4 .line3'), 0.08, {autoAlpha: 1, repeat: 1, yoyo: true}, 2.02);
        };

        // re show
        function reShow() {
            var tl = new TimelineMax({ delay: 0.2 });
            tl.to($('.page-level4'), 0.2, {onComplete: function() {
                $('.page-level4 .text1').fadeOut(400);
                $('.page-level4 .box-fulu1').fadeOut(400);
            }}, '+=1');
            tl.to($('.page-level4'), 0.2, {onComplete: function() {
                $('.page-level4 .re').fadeIn(400, function() {
                    fuluReShake4($('.page-level4 .re .box-fulu2 .fulu2'));
                    knot($('.page-level4 .re .knot'));
                });
            }}, '+=0.4');
            tl.to($('.page-level4'), 0.2, {onComplete: function() {
                $('.page-level4 .re .box-tip').fadeIn('fast', function() {
                    tipTwinkle4();
                });
            }}, '+=1');
        };
        
        // fulu shake
        function fuluReShake4(el) {
            var tl = new TimelineMax({ repeat: -1, delay: 0.2 });
            tl.to(el, 0.25, { rotation: 6, ease: Power1.easeOut, repeat: 1, yoyo: true});
            tl.to(el, 0.25, { rotation: -6, ease: Power1.easeOut, repeat: 1, yoyo: true});
        };

        // knot
        function knot(el) {
            var tl = new TimelineMax({ repeat: -1, delay: 0.2 });
             tl.to(el, 0.15, { onComplete: function() {
                el.attr('src', require('../img/level4/knot2.png'));
            }});
             tl.to(el, 0.15, { onComplete: function() {
                el.attr('src', require('../img/level4/knot3.png'));
            }});
             tl.to(el, 0.15, { onComplete: function() {
                el.attr('src', require('../img/level4/knot1.png'));
            }});
        };

        // tip
        function tipTwinkle4() {
            var tl = new TimelineMax({ repeat: -1, yoyo: true });
            tl.to($('.page-level4 .re .next'), 0.4, {autoAlpha: 1});
        };
    },
    methods: {
        prev() {
            this.$router.replace('/level3');
        },
        next() {
            this.$router.replace('/fin');
        }
    }
};
