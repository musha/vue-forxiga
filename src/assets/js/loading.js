// console.log('loading page');
import html5Loader from '../../../static/js/loader/jquery.html5Loader.min.js' // html5loader

export default {
	data() {
		return {
			source: [
                // {src: require('../img/common/bg.jpg')}
            ]
		};
	},
	mounted() {
		var context = this;

		// 预加载
        var firstLoadFiles = {
            'files': [
                // music
                {
                    'type': 'AUDIO',
                    'sources': {
                        'mp3': {
                            'source': require('../audio/op.mp3'),
                            size: 315392
                        }
                    }
                },
                // common
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/bg-tip.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/cave1.png'),
                    'size': 503
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/cave2.png'),
                    'size': 866
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/close.png'),
                    'size': 145
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/firework1.png'),
                    'size': 210
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/firework2.png'),
                    'size': 421
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/help.png'),
                    'size': 3072
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/line1.png'),
                    'size': 551
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/line2.png'),
                    'size': 691
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/line3.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/mouse.png'),
                    'size': 3072
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/next.png'),
                    'size': 933
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/open.png'),
                    'size': 158
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/prev.png'),
                    'size': 715
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/shadow.png'),
                    'size': 593
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/sugar1.png'),
                    'size': 3072
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/sugar2.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/sugar3.png'),
                    'size': 2048
                },
                // home
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/arrow.png'),
                    'size': 407
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/cloud1.png'),
                    'size': 6144
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/cloud2.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/cloud3.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/clouds1.png'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/clouds2.png'),
                    'size': 16384
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/ingot.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/lantern.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/lanterns.png'),
                    'size': 7168
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/mousefulu.png'),
                    'size': 22528
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/text.png'),
                    'size': 11264
                },
                // level1
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/bg-fulu.png'),
                    'size': 131
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/bmi.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/border.png'),
                    'size': 28672
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/boxing1.png'),
                    'size': 6144
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/boxing2.png'),
                    'size': 6144
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/boxing3.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/boxing4.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/boxing5.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/boxing6.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/boxing7.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/fire1.png'),
                    'size': 828
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/fire2.png'),
                    'size': 800
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/fire3.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/fulu1-1.png'),
                    'size': 9216
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/fulu1-2.png'),
                    'size': 13312
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/fulu1-3.png'),
                    'size': 15360
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/fulu2.png'),
                    'size': 9216
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/mouse2.png'),
                    'size': 6144
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/pao.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/shadow-mouse.png'),
                    'size': 439
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/smoke1.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/smoke2.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/smoke3.png'),
                    'size':10240
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/smoke4.png'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/smoke5.png'),
                    'size': 12288
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/text1.png'),
                    'size': 12288
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/text2.png'),
                    'size': 13312
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level1/tip.png'),
                    'size': 6144
                },
                // level2
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/arrow.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/bg-fulu.png'),
                    'size': 131
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/border.png'),
                    'size': 26624
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/cave2.png'),
                    'size': 999
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/dig1.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/dig2.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/fulu1.png'),
                    'size': 9216
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/fulu2.png'),
                    'size': 9216
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/lantern1.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/lantern2.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/lantern3.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/quote.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/sugars.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/text1.png'),
                    'size': 10240
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/text2.png'),
                    'size': 12288
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level2/tip.png'),
                    'size': 6144
                },
                // level3
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/border.png'),
                    'size': 20480
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/coin.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/crack1.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/crack2.png'),
                    'size': 3072
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/crack3.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/fulu1.png'),
                    'size': 10240
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/fulu2.png'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/hammer.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/heart1.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/heart2.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/ingot1.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/ingot2.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/line.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/mouse.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/quote.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/shadow-mouse.png'),
                    'size': 438
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/speed.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/text1.png'),
                    'size': 10240
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/text2.png'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level3/tip.png'),
                    'size': 6144
                },
                // level4
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/border.png'),
                    'size': 19456
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/fulu-re.png'),
                    'size': 9216
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/fulu1.png'),
                    'size': 10240
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/fulu2.png'),
                    'size': 16384
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/fulu3.png'),
                    'size': 17408
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/ghost.png'),
                    'size': 1024
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/knot1.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/knot2.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/knot3.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/line.png'),
                    'size': 602
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/next.png'),
                    'size': 944
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/paper1.png'),
                    'size': 407
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/paper2.png'),
                    'size': 400
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/quote.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/run1.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/run2.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/run3.png'),
                    'size': 5120
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/shadow-mouse1.png'),
                    'size': 504
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/shadow-mouse2.png'),
                    'size': 442
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/star.png'),
                    'size': 574
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/text1.png'),
                    'size': 10240
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/text2.png'),
                    'size': 12288
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/tip.png'),
                    'size': 7168
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/turn1.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/turn2.png'),
                    'size': 6144
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/level4/turn3.png'),
                    'size': 8192
                },
                // fin
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/border.png'),
                    'size': 26624
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/btn-back.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/btn-share.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/fulu.png'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/mouse.png'),
                    'size': 15360
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/ribbon.png'),
                    'size': 758
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/shadow-fulu.png'),
                    'size': 500
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/shadow-mouse.png'),
                    'size': 606
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/share.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/text.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/title1.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/fin/title2.png'),
                    'size': 8192
                },
            ]
        };

		var tl = new TimelineMax();
		tl.to($('.page-loading'), 0.4, {onComplete: function() {
            $.html5Loader({
                filesToLoad: firstLoadFiles,
                onBeforeLoad: function() {},
                onElementLoaded: function(obj, elm) {},
                onUpdate: function(percentage) {
                    // console.log(percentage);
                    $('.num').html(percentage);
                },
                onComplete: function() {
                    tl.to($('.page-loading'), 0.2, {onComplete: function() {
                        context.$router.replace('/home');
                    }}, '+=1');
                }});
        }});
	},
	methods: {
		//
	}
};