import wx from 'weixin-js-sdk'
import axios from 'axios'

window._wx = {
    config: {
        debug: false, 
        jsApiList: [
            'onMenuShareAppMessage',
            'onMenuShareTimeline',
            'hideMenuItems',
            'startRecord',
            'stopRecord',
            'onVoiceRecordEnd',
            'playVoice',
            'pauseVoice',
            'onVoicePlayEnd',
            'uploadVoice',
            'downloadVoice',
            'translateVoice'
        ],
        hideMenuItems: {
            menuList: [
                'menuItem:share:qq',
                'menuItem:share:weiboApp'
            ]
        }
    },
    shareFriend: {}, 
    shareCircle: {} 
};

function wx_init(){
    axios.get('http://api.ali.mcmas.cn/wechat.php', {
    params: {
        act:'sign',
        url: location.href.split('#')[0].toString()
    }
    }).then(function (response) {
        // console.log(response.data)
        setting(response.data)
    })
    .catch(function (error) {
        console.log(error);
    });
}

function setting(res){
    console.log(res)
    // console.log(_wx)
    wx.config({
        debug: _wx.config.debug,
        appId: res.appid, 
        timestamp: res.timestamp, 
        nonceStr: res.noncestr, 
        signature: res.signature, 
        jsApiList: _wx.config.jsApiList
    });
    wx.ready(function () {
        // console.log('ready')
        wx.hideMenuItems(_wx.config.hideMenuItems);
        wx.onMenuShareAppMessage(_wx.shareFriend);
        wx.onMenuShareTimeline(_wx.shareCircle);
    });
    wx.error(function (error) {
        console.log(error);
    });
}

_wx.shareFriend = {
    title: '金鼠喜至 福禄加持', // 分享标题
    desc: '糖友们快快点开这份秘籍，新的一年，福禄健康都鼠于你！',//分享摘要
    link: 'http://head.ali.mcmas.cn/wos/cny', // 分享链接
    imgUrl: 'http://head.ali.mcmas.cn/wos/cny/static/img/share.jpg', // 分享图标
};
Object.assign(_wx.shareCircle, _wx.shareFriend);

setTimeout(function () {
    wx_init()
}, 1000);
