import Vue from 'vue'
import Router from 'vue-router'
import loading from '@/components/loading'
import home from '@/components/home'
import level1 from '@/components/level1'
import level2 from '@/components/level2'
import level3 from '@/components/level3'
import level4 from '@/components/level4'
import fin from '@/components/fin'

Vue.use(Router)

export default new Router({
  	routes: [
	  	{
	      path: '/',
	      name: 'loading',
	      component: loading
	    },
	    {
	      path: '/home',
	      name: 'home',
	      component: home
	    },
	    {
	      path: '/level1',
	      name: 'level1',
	      component: level1
	    },
	    {
	      path: '/level2',
	      name: 'level2',
	      component: level2
	    },
	    {
	      path: '/level3',
	      name: 'level3',
	      component: level3
	    },
	    {
	      path: '/level4',
	      name: 'level4',
	      component: level4
	    },
	    {
	      path: '/fin',
	      name: 'fin',
	      component: fin
	    }
  	]
})
